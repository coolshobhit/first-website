

const loader = document.querySelector('.loader-container');
const main = document.querySelector('.main');

var dom;

// sleep time expects milliseconds
function sleep (time) {
  return new Promise((resolve) => setTimeout(resolve, time));
}

window.addEventListener('DOMContentLoaded', (event) => {
	var mainhtmldom = document.getElementById("mainhtml");
	sleep(3000).then(() => {
    	document.getElementById("container-id").outerHTML = "";
    	mainhtmldom.style.display = "block"
    	console.log('DOM fully loaded and parsed');
	});  
});

function loading(){
	var mainhtmldom = document.getElementById("mainhtml");
	mainhtmldom.style.display = "none"
    setTimeout(() => {
    
/*uncomment code below to remove loader when the page content appears*/
       //loader.style.opacity = 0;
       //loader.style.display = 'none';
     
        main.style.display = 'block';
        
        //fades in the content
        setTimeout(() => (main.style.opacity = 1), 50);
        
    }, 4000);
}

loading();
    
